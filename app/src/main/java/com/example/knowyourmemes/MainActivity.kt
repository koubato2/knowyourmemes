package com.example.knowyourmemes

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.example.knowyourmemes.ui.theme.KnowYourMemesTheme
import com.googlecode.tesseract.android.TessBaseAPI
import java.io.File

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val tess = TessBaseAPI()


       // val dataPath = File(Environment.getDataDirectory(), "tesseract").getAbsolutePath()
        val dataPath = this.filesDir.absolutePath + "/tesseract/"
        Log.d("MainActivity",dataPath)
        if (!tess.init(dataPath, "eng")) {
            // Error initializing Tesseract (wrong data path or language)
            Log.d("MainActivity","Error intializing tess")
            tess.recycle();
        }
        setContent {
            val readText = remember { mutableStateOf(TextFieldValue()) }
            KnowYourMemesTheme {
                // A surface container using the 'background' color from the theme
                Column(
                    modifier = Modifier.fillMaxWidth().fillMaxHeight(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                    )
                {
                    Text("Text is: ${readText.value.text}")
                    Spacer(Modifier.height(15.dp))
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center,
                        modifier = Modifier.fillMaxWidth()
                    ){
                        Button(
                            onClick ={ readText.value = TextFieldValue(runTes(this@MainActivity, tess)) },
                            ){ Text("Read meme") }
                    }

                }

            }
        }
    }
}



fun runTes(activity: MainActivity, tess: TessBaseAPI): String {
    // Create Tesseract instance
    val draw = ContextCompat.getDrawable(activity.applicationContext, R.drawable.test_meme)
    val pathImg = activity.filesDir.absolutePath + "/tesseract/images/test_meme.png"
    tess.setImage(File(pathImg))
    val text = tess.utF8Text
    Log.d("MainActivity", text)
    return text ?: ""

}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    KnowYourMemesTheme {
        Greeting("Android")
    }
}